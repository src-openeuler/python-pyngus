%global _empty_manifest_terminate_build 0
Name:		python-pyngus
Version:	2.3.1
Release:	1
Summary:	Callback API implemented over Proton
License:	Apache-2.0
URL:		https://github.com/kgiusti/pyngus
Source0:	https://files.pythonhosted.org/packages/f5/8e/71364e4d41e45c1698052eb948c0eb1e51ec7ca7fbb91dff828e36e39c7c/pyngus-2.3.1.tar.gz
BuildArch:	noarch
%description
Callback API implemented over Proton


%package -n python3-pyngus
Summary:	Callback API implemented over Proton
Provides:	python-pyngus
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%description -n python3-pyngus
Callback API implemented over Proton


%package help
Summary:	Development documents and examples for pyngus
Provides:	python3-pyngus-doc
%description help
Callback API implemented over Proton


%prep
%autosetup -n pyngus-2.3.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pyngus -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Jul 09 2021 openstack-sig <openstack@openeuler.org>
- Package Spec generated
